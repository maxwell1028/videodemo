import React from 'react';
import {
  AppRegistry,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import { Container, Header, Content, Button, Text } from 'native-base';

var {height, width} = Dimensions.get('window');

export default class HomePage extends React.Component {
    render() {
      const { navigate } = this.props.navigation;
      return (
        <View style={styles.container}>
            <Image style={styles.intro} source={require('./images/123.png')} />
            <Button onPress={() => this.props.navigation.navigate('VideoChat')}  info style={styles.goBtn}>
                <Text>Go VideoChat</Text>
            </Button>
        </View>
      );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width:'100%',
        height:'100%',
        alignItems:'center',
        justifyContent:'center'
    },
    intro:{
        width:width/2,
        height:width/2,
        bottom:height/4
    },
    goBtn:{
        position:'absolute',
        bottom:height/8,
        width:width-30,
        justifyContent:'center',
        alignItems:'center',
        left:15
    }
});