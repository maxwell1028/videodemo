/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import FontIcon from 'react-native-vector-icons/FontAwesome';
import HomePage from './App/HomePage';
import VideoChat from './App/VideoChat';
console.disableYellowBox = true;

export default class App extends Component<{}> {
  async componentDidMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
  }

  static navigationOptions = {
    
  };
  render() {
    return (
      <View style={styles.container}>
        <Main/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:'100%',
    height:'100%',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor: "#2E82FF",
    height: 60,
    elevation:10,
    flexDirection:'row',
    paddingTop:10
  },
});

const Main = StackNavigator({
  Home: {
    screen: HomePage,
    navigationOptions: {
      title: 'Home',
      header: (
        <View style={styles.header}>
          <View style={{flex:0.1,justifyContent:'center',alignItems:'center',paddingLeft:5,paddingTop:10}}>
          </View>
          <View style={{flex:0.8,justifyContent:'center',alignItems:'center',paddingTop:5}}>
            <Text style={{ color: 'white',fontSize: 18,fontWeight:'bold' }}>Home</Text>
          </View>
        </View>
      )
    }
  },

  VideoChat: {
    screen: VideoChat,
    navigationOptions: {
      title: 'VideoChat',
      header:null
    }
  }
},{
  initialRouteName: "Home",
  headerMode: "screen",
});
